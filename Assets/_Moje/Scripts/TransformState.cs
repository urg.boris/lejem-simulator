﻿using System;
using UnityEngine;
[Serializable]
public struct TransformState
{
    public Vector3 pos;
    public Vector3 rot;
    public Vector3 scale;

    public TransformState(Vector3 pos, Vector3 rot, Vector3 scale)
    {
        this.pos = pos;
        this.rot = rot;
        this.scale = scale;
    }
};