using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using _Moje;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.Rendering;
using Plane = UnityEngine.Plane;
using Quaternion = UnityEngine.Quaternion;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;
using Vector4 = UnityEngine.Vector4;

[Serializable]
public class LevelSettings
{
    public TransformState handTrans;
    public TransformState glassTrans;
    public Vector4 planeLimits;
    public Vector3 pointOnPlain;
}

public struct FingerBone
{
    public readonly float Radius;
    public readonly float Height;

    public FingerBone(float radius, float height)
    {
        Radius = radius;
        Height = height;
    }

    public Vector3 GetCenter(bool isLeftHand)
    {
        return new Vector3(((isLeftHand) ? -1 : 1) * Height / 2.0f, 0, 0);
    }
};

[Serializable]
public class FingerPart
{
    public GameObject GO;
    public CapsuleCollider CC;
    public Rigidbody RB;
    public Quaternion origRotation;

    public FingerPart(GameObject GO, CapsuleCollider CC, Rigidbody RB, Quaternion origRotation)
    {
        this.GO = GO;
        this.CC = CC;
        this.RB = RB;
        this.origRotation = origRotation;
    }
}

[Serializable]
public class FingerChain
{
    public FingerPart root;
    public FingerPart middle;
    public FingerPart tip;


    public FingerChain(FingerPart root, FingerPart middle, FingerPart tip)
    {
        this.root = root;
        this.middle = middle;
        this.tip = tip;
    }

    public Transform[] GetTransformObjects()
    {
        Transform[] transforms = new Transform[3]
        {
            root.GO.transform,
            middle.GO.transform,
            tip.GO.transform
        };
        return transforms;
    }
}


public class HandController : MonoBehaviour
{
    public LevelSettings[] LevelSettings;
    [HideInInspector] public LevelSettings currentLevel;
    public DialogBox DialogBox;

    public int levelIndex = 0;

    //public GameObject ScaleRatioRef;
    private float scaleRatio;
    private readonly FingerBone Phalanges = new FingerBone(0.01f, 0.03f);

    private readonly FingerBone Metacarpals = new FingerBone(0.00007f, 0.0005f);

    // public Transform finger1base;
    //  public Transform finger5base;
    //public Transform[] fingerParts;
    // private float finger1baseRot;
    public GlassController GlassController;
    public HeadController HeadController;

    public List<TransformState> origFingersTransformStates;

    public Transform BaseFingerBoneTransform;
    public Transform handTransform;
    [Tooltip(" xmin, xmax, ynim, ymax")] public Vector4 handBounds;

    public FingerChain thumbChains;

    public FingerChain[] fingerChains;
    /* public Transform F1P1RB;
     public Transform F1P2RB;
     public Transform F1P3RB;*/

    public Camera _camera;

    //  public Vector3 startingPos;

    public float minY = 0.718f;
    public SkinnedMeshRenderer skinnedMeshRenderer;

    // public bool grabRutineOn = true;

    private Coroutine GrabbingCorutine;

    //  public HandState currentState;
    public Animator Animator;
    public AnimationClip MoveHandAnimationState;
    private int MoveHandAnimFrames;
    private string _stateMoveToGrabbingName = "moveToGrabbingPos";

    private string _stateDrinkName = "drink";
    private Vector3[] randomDiretionsForFingers;
    private Quaternion[] randomRotationsForFingers;

    //private Vector3 StartMouseClickPos;
    //private float reachingFinalHandPosT;
    public static readonly Vector3 FAKE_EMPTY_VECTOR = new Vector3(666, 666, 666);
    private Vector3 origCamPos;

    private TransformState origLevelTransformState;
    //  private Vector3 origRotation;


    public Transform DrinkPivotRot;

    public AHandMoveState HandMoveState;
    public AHandGrabState HandGrabState;
    public AHandDrinkState HandDrinkState;

    private ALevelState currentLevelState;
    //private bool grabFinnished = false;
/*
    public void SetState(HandState handState)
    {
        currentState?.Close();
        currentState = handState;
        currentState.Start();
    }
    */

    public bool IsHoldindGlass()
    {
        return GlassController.isHolding;
    }

    public void SetLevelState(ALevelState levelState)
    {
        currentLevelState = levelState;
    }

    public void SetMoveState(AHandMoveState handState)
    {
        HandMoveState?.Close();
        HandMoveState = handState;
        HandMoveState.Start();
    }

    public void SetGrabState(AHandGrabState handState)
    {
        HandGrabState?.Close();
        HandGrabState = handState;
        HandGrabState.Start();
    }

    public void SetDrinkState(AHandDrinkState handState)
    {
        HandDrinkState?.Close();
        HandDrinkState = handState;
        HandDrinkState.Start();
    }

    // Start is called before the first frame update
    void Start()
    {
        RememerOrigTransformStates();
        GlassController.transform.position = new Vector3(0.33f, 0.822f, -8.625f);

        RestartLevels();
    }

    void RestartLevels()
    {
        levelIndex = 0;
        StartLevel();
        DialogBox.StartLevel(levelIndex);
    }

    public void RememberLevelTransform()
    {
        var transform1 = transform;
        origLevelTransformState = new TransformState(transform1.position, transform1.rotation.eulerAngles,
            transform1.localScale);
    }

    void SetStatesForL1()
    {
        SetMoveState(new L1HandStateMove0(this));
        SetGrabState(new L1HandStateGrab0(this));
        SetDrinkState(new L1HandStateDrink0(this));
    }

    void SetStatesForL2()
    {
        SetMoveState(new L2HandStateMove0(this));
        SetGrabState(new L1HandStateGrab0(this));
        SetDrinkState(new L1HandStateDrink0(this));
    }

    void SetStatesForL3()
    {
        SetMoveState(new L3HandStateMove0(this));
        SetGrabState(new L1HandStateGrab0(this));
        SetDrinkState(new L1HandStateDrink0(this));
    }

    void StartLevel()
    {
        currentLevel = LevelSettings[levelIndex];
        LoadTranformFromStuct(GlassController.transform, currentLevel.glassTrans);
        GlassController.RememberLevelTransform();
        LoadTranformFromStuct(transform, currentLevel.handTrans);
        RememberLevelTransform();
        switch (levelIndex)
        {
            case 0:
                SetStatesForL1();
                break;
            case 1:
                SetStatesForL2();
                break;
            case 2:
                SetStatesForL3();
                break;
            default:
                Debug.LogError("no lev " + levelIndex);
                break;
        }
    }

    void RememerOrigTransformStates()
    {
        origCamPos = _camera.transform.rotation.eulerAngles;
        //  origRotation = transform1.rotation.eulerAngles;
        // finger1baseRot = finger1base.localRotation.eulerAngles.x;
        // startingPos = handTransform.position;
        origFingersTransformStates = new List<TransformState>();

        RememberFingerTransforms();
    }

    void ResetState()
    {
        ToggleFingersLiquidity(true);
        LoadTranformFromStuct(handTransform.transform, currentLevel.handTrans);
        ResetFingerTransforms();
        GlassController.ResetState();
        HeadController.ResetState();
        //Animator.Play("Idle");
        //   reachingFinalHandPosT = 0;
        //   grabFinnished = false;
        StartLevel();
        DialogBox.ResetAgain();
        //SetState(new HandMoveL1PreGrab(this));
    }

    public void RestartGame()
    {
        ToggleFingersLiquidity(true);
        LoadTranformFromStuct(handTransform.transform, currentLevel.handTrans);
        ResetFingerTransforms();
        GlassController.ResetState();
        HeadController.ResetState();
        DialogBox.ResetState();
        RestartLevels();
    }


    bool NoMouseButtonIsDown()
    {
        return !Input.GetMouseButton(0) && !Input.GetMouseButton(1) && !Input.GetMouseButton(2);
    }


    void Update()
    {
        // bool isAnyMouseButtonDown = false;
        //BendFingers();
        /*       if (Input.GetKeyUp(KeyCode.C))
               {
                   SetColliders();
               }*/

        if (Input.GetKeyUp(KeyCode.R))
        {
            ResetState();
        }

        if (Input.GetKeyUp(KeyCode.P))
        {
            StartNextLevel();
            //Debug.Log("plus");
            /*  levelIndex++;
              if (levelIndex >= LevelSettings.Length)
              {
                  levelIndex = LevelSettings.Length - 1;
              }
              ResetState();
              DialogBox.StartLevel(levelIndex);*/
        }

        if (Input.GetKeyUp(KeyCode.O))
        {
            StartPrevLevel();
        }

        //currentState?.Update();
        HandMoveState?.Update();
        HandGrabState?.Update();
        HandDrinkState?.Update();

        if (NoMouseButtonIsDown())
        {
//            Debug.Log("NoMouseButtonIsDown");
            var mousePos = Input.mousePosition;

            // Debug.Log(mousePos);
            mousePos = _camera.ScreenToViewportPoint(mousePos) * 2 - new Vector3(1, 1, 1);
            mousePos.y *= -1;
            // Debug.Log(mousePos);
            Quaternion newRot = Quaternion.Euler(origCamPos + new Vector3(20 * mousePos.y, mousePos.x * 20, 0));

            _camera.transform.rotation = Quaternion.Lerp(_camera.transform.rotation, newRot, 0.1f);
        }
    }
/*
    private void FixedUpdate()
    {
        currentState?.FixedUpdate();
    }
*/
    /*
    public void StartGrabbing()
    {
        grabRutineOn = true;
    }*/
/*
    void StartHandPosFromAnimation()
    {
        Animator.Play(_stateMoveToGrabbingName);
        Animator.speed = 0;
    }*/
/*
    public void StartHandDrinkAnimation()
    {
        Animator.Play(_stateDrinkName);
        Animator.speed = 0;
    }*/

    public TransformState GetGlassHandRelativeTransform()
    {
        return new TransformState(
            GlassController.transform.position - handTransform.position,
            GlassController.transform.rotation.eulerAngles - handTransform.rotation.eulerAngles,
            Vector3.one
        );
    }


    public void SetHandPosFromAnimation(float t)
    {
        //Debug.Log("t="+t);
        t = Mathf.Clamp01(t);
        // var normTime = Mathf.Lerp(0, MoveHandAnimFrames, t);
        Animator.Play(_stateMoveToGrabbingName, 0, t);
        Animator.speed = 0.0f;
    }

    public void SetHandDrinkAnimation(float t)
    {
        //Debug.Log("t="+t);
        t = Mathf.Clamp01(t);
        // var normTime = Mathf.Lerp(0, MoveHandAnimFrames, t);

        Animator.Play(_stateDrinkName, 0, t);
        Animator.speed = 0.0f;
    }

    private void RememberFingerTransforms()
    {
        foreach (var fingerChain in fingerChains)
        {
            fingerChain.root.origRotation = fingerChain.root.GO.transform.localRotation;
            fingerChain.middle.origRotation = fingerChain.middle.GO.transform.localRotation;
            fingerChain.tip.origRotation = fingerChain.tip.GO.transform.localRotation;
            var fingerChainTransformObjects = fingerChain.GetTransformObjects();
            foreach (var transformObject in fingerChainTransformObjects)
            {
                origFingersTransformStates.Add(new TransformState(
                    transformObject.localPosition,
                    transformObject.localRotation.eulerAngles,
                    transformObject.localScale
                ));
            }
        }

        var thumbTransformObjects = thumbChains.GetTransformObjects();
        thumbChains.root.origRotation = thumbChains.root.GO.transform.localRotation;
        thumbChains.middle.origRotation = thumbChains.middle.GO.transform.localRotation;
        thumbChains.tip.origRotation = thumbChains.tip.GO.transform.localRotation;
        foreach (var transformObject in thumbTransformObjects)
        {
            origFingersTransformStates.Add(new TransformState(
                transformObject.localPosition,
                transformObject.localRotation.eulerAngles,
                transformObject.localScale
            ));
        }
    }

    public void LoadTranformFromStuct(Transform transform, TransformState transformState)
    {
        transform.localPosition = transformState.pos;
        transform.localRotation = Quaternion.Euler(transformState.rot);
        transform.localScale = transformState.scale;
    }

    private void ResetFingerTransforms()
    {
        var index = 0;

        foreach (var fingerChain in fingerChains)
        {
            LoadTranformFromStuct(fingerChain.root.GO.transform, origFingersTransformStates[index]);
            fingerChain.root.RB.velocity = Vector3.zero;
            index++;
            LoadTranformFromStuct(fingerChain.middle.GO.transform, origFingersTransformStates[index]);
            fingerChain.middle.RB.velocity = Vector3.zero;
            index++;
            LoadTranformFromStuct(fingerChain.tip.GO.transform, origFingersTransformStates[index]);
            fingerChain.tip.RB.velocity = Vector3.zero;
            index++;
        }


        LoadTranformFromStuct(thumbChains.root.GO.transform, origFingersTransformStates[index]);
        thumbChains.root.RB.velocity = Vector3.zero;
        index++;
        LoadTranformFromStuct(thumbChains.middle.GO.transform, origFingersTransformStates[index]);
        thumbChains.middle.RB.velocity = Vector3.zero;
        index++;
        LoadTranformFromStuct(thumbChains.tip.GO.transform, origFingersTransformStates[index]);
        thumbChains.tip.RB.velocity = Vector3.zero;
        index++;
    }


    public void SetColliders()
    {
        List<FingerChain> fingerChains = new List<FingerChain>();

        foreach (Transform rootFingerPart in BaseFingerBoneTransform)
        {
            FingerPart fingerPartRoot = null;
            FingerPart fingerPartMiddle = null;
            FingerPart fingerPartTip = null;

            var bones = rootFingerPart.GetComponentsInChildren<Transform>();
            foreach (Transform fingerPartTransorm in bones)
                if (!fingerPartTransorm.name.Contains("_end") && !fingerPartTransorm.name.Contains("_base"))
                {
                    //Debug.Log("creating bone for " + bone.name);
                    var fingerPart = CreateCollider(fingerPartTransorm);
                    if (fingerPartTransorm.name.Contains("_prox"))
                    {
                        fingerPartRoot = fingerPart;
                    }

                    if (fingerPartTransorm.name.Contains("_inter"))
                    {
                        fingerPartMiddle = fingerPart;
                    }

                    if (fingerPartTransorm.name.Contains("_dist"))
                    {
                        fingerPartTip = fingerPart;
                    }
                }

            var fingerChain = new FingerChain(fingerPartRoot, fingerPartMiddle, fingerPartTip);
            fingerChains.Add(fingerChain);
        }

        this.fingerChains = fingerChains.ToArray();
    }
    //var fingerChain = new FingerChain()

    public void StopHandsForDrinking()
    {
        //SetGrabState(new L1HandStateGrab2());
        SetMoveState(new L1HandStateMove2(this));
    }

    public void StartNextLevel()
    {
        levelIndex++;
        if (levelIndex >= LevelSettings.Length)
        {
            FinnishGame();
            return;
        }

        ResetState();
        DialogBox.StartLevel(levelIndex);
    }

    public void StartPrevLevel()
    {
        //Debug.Log("minus");
        levelIndex--;
        if (levelIndex < 0)
        {
            levelIndex = 0;
        }

        ResetState();
        DialogBox.StartLevel(levelIndex);
    }

    void FinnishGame()
    {
        StopHandsForDrinking();
        DialogBox.BlackoutWithCallback(RestartGame);
    }



    private FingerPart CreateCollider(Transform transform)
    {
        var toPos = transform.GetComponentsInChildren<Transform>()[1].position;
        // Debug.Log("toPos" + transform.name);
        //  Debug.Log(toPos);
        var prevCollider = transform.GetComponent<CapsuleCollider>();
        if (prevCollider)
        {
            DestroyImmediate(prevCollider);
        }

        var prevRB = transform.GetComponent<Rigidbody>();
        if (prevRB)
        {
            DestroyImmediate(prevRB);
        }

        CapsuleCollider collider = transform.gameObject.AddComponent<CapsuleCollider>();
        // var scale = 0.001f;
        collider.radius = Metacarpals.Radius;
        var height = Vector3.Distance(transform.position, toPos) * scaleRatio;
        collider.center = new Vector3(0, height / 2, -Metacarpals.Radius / 2);
        collider.height = height;

        collider.direction = 1;

        Rigidbody rigidBody = transform.gameObject.AddComponent<Rigidbody>();
        rigidBody.isKinematic = true;
        rigidBody.useGravity = false;
        return new FingerPart(transform.gameObject, collider, rigidBody, transform.localRotation);
    }


    public void FloatingFinger()
    {
        var i = 0;
        foreach (var fingerChain in fingerChains)
        {
            i++;
            //var quatScale = Quaternion.Euler(-Mathf.Sin(Time.time + i)*Time.deltaTime, 0, 0);
            fingerChain.root.GO.transform.localRotation =
                fingerChain.root.origRotation *
                Quaternion.Euler(10 * Mathf.Sin(Time.time + i * 5), 0, 0);
        }
    }

    void ToggleFingersLiquidity(bool isFirm)
    {
        foreach (var fingerChain in fingerChains)
        {
            fingerChain.tip.RB.isKinematic = isFirm;
            fingerChain.middle.RB.isKinematic = isFirm;
            fingerChain.root.RB.isKinematic = isFirm;
        }

        thumbChains.tip.RB.isKinematic = isFirm;
        thumbChains.middle.RB.isKinematic = isFirm;
        thumbChains.root.RB.isKinematic = isFirm;
    }

    public void SetRandomDirectionsForFinders()
    {
        ToggleFingersLiquidity(false);
        randomDiretionsForFingers = new Vector3[4];
        randomRotationsForFingers = new Quaternion[4];
        for (int i = 0; i < fingerChains.Length; i++)
        {
            randomDiretionsForFingers[i] = Random.insideUnitSphere.normalized;
            randomRotationsForFingers[i] = Random.rotation;
        }
    }

    public void DrunkenFinger()
    {
        var i = 0;

        foreach (var fingerChain in fingerChains)
        {
            fingerChain.tip.GO.transform.localRotation = Quaternion.Slerp(
                fingerChain.tip.GO.transform.localRotation,
                randomRotationsForFingers[i],
                0.002f
            );
            //var quatScale = Quaternion.Euler(-Mathf.Sin(Time.time + i)*Time.deltaTime, 0, 0);
            //fingerChain.tip.GO.transform.position += fingerChain.tip.GO.transform.forward*0.1f*Time.deltaTime;//randomDiretionsForFingers[i]
            i++;
            /*  fingerChain.root.origRotation *
              Quaternion.Euler(10 * Mathf.Sin(Time.time + i * 5), 0, 0);*/
        }
    }

    public bool IsInGrabbingMove()
    {
        //   Debug.Log("graging="+HandGrabState.IsInGrabbingMove());
        if (HandGrabState == null)
        {
            return false;
        }

        return HandGrabState.IsInGrabbingMove();
    }

    public void GlassGrabbed()
    {
        //StopGrabbing();
        //LockGlassToHand();
        SetMoveState(new L1HandStateMove1(this));
        SetGrabState(new L1HandStateGrab2(this));
        //SetState(new HandMoveL1Drink(this));
    }
/*
    public void HandReachedMouth()
    {
        SetDrinkState(new L1HandStateDrink2(this));
        //SetState(new HandMoveMouth(this));
    }*/
/*
    void StopGrabbing()
    {
        grabRutineOn = false;
      //  grabFinnished = true;
    }*/

    public void LockGlassToHand()
    {
        //HeadController.ActivateMovableFace();
        GlassController.LockGlassToHand();
    }

    public void SetThumbContact()
    {
        HandGrabState.thumbContact = true;
    }

    public void SetFingerContact()
    {
        HandGrabState.fingerContact = true;
    }

    public void GrabRutine(bool fingers, bool thumb)
    {
        var i = 0;
        var quat = Quaternion.Euler(-60, 0, 0);
        float lerpSpeed = 0.05f;
//        Debug.Log(HandGrabState.fingerContact);
        if (!HandGrabState.fingerContact)
        {
            foreach (var fingerChain in fingerChains)
            {
                i++;
                //var quatScale = Quaternion.Euler(-Mathf.Sin(Time.time + i)*Time.deltaTime, 0, 0);
                fingerChain.root.GO.transform.localRotation = Quaternion.Lerp(
                    fingerChain.root.GO.transform.localRotation,
                    quat, lerpSpeed);
                fingerChain.middle.GO.transform.localRotation = Quaternion.Lerp(
                    fingerChain.middle.GO.transform.localRotation,
                    quat, lerpSpeed);
                fingerChain.tip.GO.transform.localRotation = Quaternion.Lerp(fingerChain.tip.GO.transform.localRotation,
                    quat, lerpSpeed);
                //fingerChain.root.origRotation * Quaternion.Euler(50*Mathf.Sin(Time.time), 0, 0); //Rotate((fingerChain.root.GO.transform.rotation * quatScale).eulerAngles);// .MoveRotation(fingerChain.root.RB.rotation * quatScale);*/
                // fingerChain.root.RB.MoveRotation( fingerChain.root.origRotation * Quaternion.Euler(50*Mathf.Sin(Time.time), 0, 0));
            }
        }

        if (!HandGrabState.thumbContact)
        {
            //thumb
            var thumbuat = Quaternion.Euler(0, 0, 60);
            // thumbChains.root.GO.transform.localRotation = Quaternion.Lerp(thumbChains.root.GO.transform.localRotation,
            //   quat, 0.02f);
            thumbChains.middle.GO.transform.localRotation = Quaternion.Lerp(
                thumbChains.middle.GO.transform.localRotation,
                thumbuat, lerpSpeed);
            thumbChains.tip.GO.transform.localRotation = Quaternion.Lerp(thumbChains.tip.GO.transform.localRotation,
                thumbuat, lerpSpeed);

            if (Math.Abs(thumbChains.tip.GO.transform.localRotation.eulerAngles.z - 60) < 5)
            {
                FailMissedGlass();
            }
        }
    }

    void FailMissedGlass()
    {
        SetMoveState(new L1HandStateMove1(this));
        SetLevelState(new LevelStateMissedGlass(this));
        //SetGrabState(new L1HandStateGrab2(this));
        //SetState(new HandMoveStoppedGrab(this));
    }

    public void FailSpilledGlass()
    {
        DialogBox.Spilled(ResetState);
        SetLevelState(new LevelStateSpilledGlass(this));
        //SetState(new HandMoveStoppedGrab(this));
    }

    public void FailGlassFalled()
    {
        Debug.Log("falled");
        // SetState(new HandMoveStoppedGrab(this));
    }

    public void HandReachedMouth()
    {
        SetDrinkState(new L1HandStateDrink2(this));
        if (IsHoldindGlass())
        {
            StartCoroutine(WaitBeforeNextLevel());
        }
        else
        {
            
        }
    }

    IEnumerator WaitBeforeNextLevel()
    {
        yield return new WaitForSeconds(2);
        StartNextLevel();
    }

// Update is called once per frame
}