using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadController : MonoBehaviour
{
    public float speed;
    public Material FaceMaterial;
    private Vector3 OrigPos; 
    private Vector3 velocity = Vector3.zero;
    public HandController HandController;
    

    private bool movable = false;
    // Start is called before the first frame update
    void Start()
    {
        OrigPos = transform.localPosition;
        this.gameObject.SetActive(false);
    }

    public void ResetState()
    {
        movable = true;
        this.gameObject.SetActive(false);
        transform.localPosition = OrigPos;
    }

    public void ActivateMovableFace()
    {
        movable = true;
        this.gameObject.SetActive(true);
    }
/*
    private void OnCollisionEnter(Collision other)
    {
        Debug.Log(other.collider.name);
        if (other.collider.CompareTag("glass"))
        {
            movable = false;
            StartCoroutine(DrinkForAwhile());
        }
    }*/

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("glass"))
        {
            Debug.Log("col="+other.name);
            movable = false;
            StartCoroutine(DrinkForAwhile());
        }
    }

    IEnumerator DrinkForAwhile()
    {
        HandController.StopHandsForDrinking();
        yield return new WaitForSeconds(2);
        //HandController.StartNextLevel();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (movable)
        {
            var posAd = Vector3.zero;
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                posAd += Vector3.left;
            }

            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                posAd += Vector3.right;
            }

            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                posAd += Vector3.up;
            }

            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                posAd += Vector3.down;
            }

/*
        destinationPos = posAd;
      //  transform.localPosition = Vector3.SmoothDamp( transform.localPosition, destinationPos, ref velocity, speed);
      var localPosition = transform.localPosition;
      localPosition += Vector3.Lerp(localPosition, destinationPos, 0.3f);// */
            transform.localPosition += posAd * Time.deltaTime * speed;
        }
    }
}
