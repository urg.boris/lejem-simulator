﻿using UnityEngine;

namespace _Moje
{
    public class LevelStateSpilledGlass:ALevelState
    {
        public LevelStateSpilledGlass(HandController handController) : base(handController)
        {
        }

        public override void GetDebug()
        {
            {
                Debug.Log("spilled");
            }
        }
    }
}