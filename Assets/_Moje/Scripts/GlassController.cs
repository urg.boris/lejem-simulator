using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class GlassController : MonoBehaviour
{
    public int collisionCount;

    public HandController HandController;

    private int currentCollisionCount;

    public Rigidbody rb;

    public Collider handCollider;
    public Collider bottomCollider;
    public Material LiquidMaterial;
    public Vector2 liquidLimitsRotations;
    public Vector2 liquidLimitTs;
    private static readonly int Amount01 = Shader.PropertyToID("_Amount01");
    private float minLiquidLevelreached;
    private bool fingerTouch;
    private bool thumbTouch;
    public ParticleSystem cakycaky;
    public Transform CakyPivot;
    private float cakystart;
    public float cakyShowTime;
    public float cakyAngle;
    private float angleReached;
    private TransformState origTransformState;

    private bool isSpilled;
    public bool isHolding;

    // Start is called before the first frame update
    void Start()
    {
        ResetLiquid();
    }

    public void RememberLevelTransform()
    {
        var transform1 = transform;
        origTransformState = new TransformState(transform1.position, transform1.rotation.eulerAngles,
            transform1.localScale);
    }

    public void ResetState()
    {
        ResetLiquid();
        isHolding = false;
        fingerTouch = false;
        thumbTouch = false;
        isSpilled = false;
        transform.parent = null;
        HandController.LoadTranformFromStuct(transform, origTransformState);
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        
        /*glassRB.MovePosition(origTransformState.pos);
        glassRB.MoveRotation(Quaternion.Euler(origTransformState.rot));*/
        //grabRutineOn = false;
        rb.isKinematic = false;
        rb.useGravity = true;
        ActivateColliders();
    }

    private void ResetLiquid()
    {
        LiquidMaterial.SetFloat(Amount01, liquidLimitTs.x);
        minLiquidLevelreached = liquidLimitTs.x;
        cakycaky.gameObject.SetActive(false);
    }

    public void LockGlassToHand()
    {
        isHolding = true;
        rb.isKinematic = true;
        rb.useGravity = false;
        transform.parent = HandController.handTransform;
    }

    // Update is called once per frame
    void Update()
    {
       // float angle = Quaternion.Angle(transform.rotation, Quaternion.Euler(0,0,0));
        float angle = Vector3.Angle(transform.up, Vector3.up);
        // float angleX = Quaternion.Angle(transform.rotation, Quaternion.Euler(90,0,0));
//Debug.Log(aa);
        if (!isHolding)
        {
            if (angle > 85 && !isSpilled)
            {
                GlassSpilled();
            }
            
            if (angle >= cakyAngle && !isSpilled)
            {
                if (angle > angleReached)
                {
                    cakystart = Time.time;
                    cakycaky.gameObject.SetActive(true);
                    angleReached = angle;
                }
            }
            if (Time.time - cakystart > cakyShowTime && cakycaky.gameObject.activeSelf)
            {
                cakycaky.gameObject.SetActive(false);
            }
        }
        
        var dirFromCenter = CakyPivot.position - transform.position;
        var dirInXPlain = new Vector3(dirFromCenter.x, 0, dirFromCenter.z);
        var cakyYAngle = 180 - Vector2.SignedAngle(Vector2.up, new Vector2(dirInXPlain.x, dirInXPlain.z));
        CakyPivot.localRotation = Quaternion.Euler(0, -transform.rotation.eulerAngles.y+cakyYAngle, 0);
      
        if (angle >= liquidLimitsRotations.y)
        {
            minLiquidLevelreached = 0;
        }

//        Debug.Log(angle);


        var liquidLimitT = Mathf.InverseLerp(liquidLimitsRotations.x, liquidLimitsRotations.y, angle);
//        Debug.Log(liquidLimitT);
        var t = Mathf.Lerp(liquidLimitTs.x, liquidLimitTs.y, liquidLimitT);



        if (t < minLiquidLevelreached)
        {
            minLiquidLevelreached = t;
        }




        LiquidMaterial.SetFloat(Amount01, minLiquidLevelreached);

        /*  
      var rotation = transform.rotation;
      float angle = rotation.eulerAngles.magnitude;
      if (angle > 180)
      {
          angle = 360 - angle;
      }*/
        // Debug.Log(angle);
    }

    void GlassSpilled()
    {
        isSpilled = true;
        HandController.FailSpilledGlass();
    }

    public void DeactivateColliders()
    {
       // handCollider.enabled = false;
        bottomCollider.enabled = false;
    }

    public void ActivateColliders()
    {
//        Debug.Log("col active");
        handCollider.enabled = true;
        bottomCollider.enabled = true;
    }



    private void OnCollisionStay(Collision collision)
    {
        // Debug.Log(HandController.IsInGrabbingMove());
        if (HandController.IsInGrabbingMove())
        {
            foreach (ContactPoint contact in collision.contacts)
            {
                if (contact.otherCollider.CompareTag("finger"))
                {
                    fingerTouch = true;
                    //continue;
                }

                if (contact.otherCollider.CompareTag("thumb"))
                {
//                    Debug.Log(collision.gameObject.name);
  //                  Debug.Log("Thumb "+contact.otherCollider.gameObject.name);
                    thumbTouch = true;
                }

                //Debug.DrawRay(contact.point, contact.normal, Color.white);
            }
/*
            if (fingerTouch)
            {
                HandController.SetFingerContact();
            }
            if (thumbTouch)
            {
                HandController.SetThumbContact();
            }*/

            if (thumbTouch && fingerTouch)
            {
                HandController.SetFingerContact();
                HandController.SetThumbContact();
                HandController.GlassGrabbed();
                DeactivateColliders();
                
            }
        }


        // Debug.Log("tagged col" + currentCollisionCount);
    }
}