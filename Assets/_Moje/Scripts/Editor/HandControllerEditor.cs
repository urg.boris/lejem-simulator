using UnityEngine;
using System.Collections;
using System;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(HandController))]
public class HandControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Create Colliders"))
        {
            (target as HandController).SetColliders();
        }
        
    }


    public void OnSceneGUI()
    {

    }
}
#endif