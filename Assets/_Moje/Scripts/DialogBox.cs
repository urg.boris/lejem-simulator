using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class DialogBox : MonoBehaviour
{

    public TMP_Text text;

    public GameObject Container;
    public HandController HandController;

    private string[] TEXT_DEEPTALKS = new[]
    {
        "Barman: Tak si dej panáka a povídej...",
        "Povídáte si o vesmíru a těch dalších věcech...",
        "Náhodně generujete slova a pazvuky..."
    };

    private string TEXT_FAIL_SPILLED = "Barman: Kurva, nelej to tady..";
    private string TEXT_HATE = "Barman: Seš vožralej, nebo co?";
    private int resetCount;

    private string TEXT_FAIL_DRUNK = "Barman: Tyvole, podívej se na sebe, seš uplně rozteklej.. di domu.";

    private Action currentCallback;

    private Coroutine showTextCorut;

    public Image Background;
    
    // Start is called before the first frame update
    void Start()
    {
        //HideText();
        StartCoroutine(CoFromBlack());
    }

    public void ResetState()
    {
        HideText();
        StartCoroutine(CoFromBlack());
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            StopCorutIfNeeded();
        }
    }

    void StopCorutIfNeeded()
    {
        if (showTextCorut != null)
        {
            StopCoroutine(showTextCorut);
            showTextCorut = null;
            FinnishDialog();
        }
    }

    public void ResetAgain()
    {
        resetCount++;
        if (resetCount > 2)
        {
            resetCount = 0;
            if (HandController.levelIndex == HandController.LevelSettings.Length - 1)
            {
                currentCallback = null;
                ShowText(TEXT_FAIL_DRUNK);
                //currentCallback = HandController.RestartGame;
                CleanStartCoroutine(4);
                BlackoutWithCallback(HandController.RestartGame);
            }
            else
            {
                ShowText(TEXT_HATE);
                currentCallback = null;
                CleanStartCoroutine(2);
            }
        }
    }

    void FinnishDialog()
    {
        HideText();
        if (currentCallback != null)
        {
            currentCallback();
        }
    }
/*
    public void Init(Action callback=null)
    {
        ShowText(TEXT_INIT);
        currentCallback = callback;
        showTextCorut = StartCoroutine(ShowTextForWhile(3));
    }*/
    public void BlackoutWithCallback(Action callback)
    {
        StartCoroutine(CoToBlack(callback));
    }

    IEnumerator CoToBlack(Action callback=null)
    {
        var initialColor  = new Color(0, 0, 0, 0);
        var targetColor = new Color(0, 0, 0, 1);
        Background.color = initialColor;
        float a = 0;
        float elapsedTime = 0f;
        float fadeDuration = 1;
        while (elapsedTime < fadeDuration)
        {
            elapsedTime += Time.deltaTime;
            Background.color = Color.Lerp(initialColor, targetColor, elapsedTime / fadeDuration);
            yield return null;
        }

        yield return new WaitForSeconds(2);

        if (callback != null) callback();
    }
    IEnumerator CoFromBlack(Action callback=null)
    {
        var initialColor  = new Color(0, 0, 0, 1);
        var targetColor = new Color(0, 0, 0, 0);
        Background.color = initialColor;
        float a = 0;
        float elapsedTime = 0f;
        float fadeDuration = 1;
        while (elapsedTime < fadeDuration)
        {
            elapsedTime += Time.deltaTime;
            Background.color = Color.Lerp(initialColor, targetColor, elapsedTime / fadeDuration);
            yield return null;
        }

        if (callback != null) callback();
    }
    
    void CleanStartCoroutine(float seconds = 5)
    {
       // currentCallback = null;
       if (showTextCorut != null)
       {
           StopCoroutine(showTextCorut);
       }

       showTextCorut = StartCoroutine(ShowTextForWhile(seconds));
    }
    
    public void StartLevel(int levelIndex, Action callback=null)
    {
        ShowText(TEXT_DEEPTALKS[levelIndex]);
        currentCallback = callback;
        CleanStartCoroutine(3);
    }
    
    public void Spilled(Action callback=null)
    {
        ShowText(TEXT_FAIL_SPILLED);
        currentCallback = callback;
        CleanStartCoroutine(3);
    }
    
    public void Drunk(Action callback=null)
    {
        ShowText(TEXT_FAIL_DRUNK);
        currentCallback = callback;
        CleanStartCoroutine(3);
    }

    IEnumerator ShowTextForWhile(float seconds = 5)
    {
        yield return new WaitForSeconds(seconds);
        FinnishDialog();
    }

    void ShowText(string dialogText)
    {
        text.text = dialogText;
        Container.SetActive(true);
    }
    
    void HideText()
    {
        text.text = "";
        Container.SetActive(false);
    }


}
