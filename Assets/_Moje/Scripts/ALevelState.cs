﻿namespace _Moje
{
    public abstract class ALevelState
    {
        private HandController HandController;

        public ALevelState(HandController handController)
        {
            HandController = handController;
        }

        public abstract void GetDebug();

    }
}