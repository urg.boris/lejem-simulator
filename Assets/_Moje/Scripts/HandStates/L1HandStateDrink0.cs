﻿using UnityEngine;

namespace _Moje
{
    public class L1HandStateDrink0:AHandDrinkState
    {
        public L1HandStateDrink0(HandController handController) : base(handController)
        {
        }

        public override void Update()
        {
            //second
            if (Input.GetMouseButtonDown(1))
            {
                handController.SetDrinkState(new L1HandStateDrink1(handController));
                handController.SetMoveState(new L1HandStateMove1(handController));
            }
        }


    }
}