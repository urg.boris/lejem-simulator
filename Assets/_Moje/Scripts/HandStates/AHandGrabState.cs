﻿using Newtonsoft.Json.Serialization;

public abstract class AHandGrabState: AHandMove
{
    public bool thumbContact;
    public bool fingerContact;
    protected AHandGrabState(HandController handController) : base(handController)
    {
    }

    public virtual bool IsInGrabbingMove()
    {
        return false;
    } 
}