﻿using Newtonsoft.Json.Serialization;

public abstract class AHandMove
{
    protected HandController handController;

    public AHandMove(HandController handController)
    {
        this.handController = handController;
    }

    public virtual void Start()
    {
        
    }

    public virtual void Update()
    {
        
    }
    


    public virtual void Close()
    {
        
    }
}