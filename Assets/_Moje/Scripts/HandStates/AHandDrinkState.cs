﻿using Newtonsoft.Json.Serialization;

public abstract class AHandDrinkState: AHandMove
{
    protected AHandDrinkState(HandController handController) : base(handController)
    {
    }
}