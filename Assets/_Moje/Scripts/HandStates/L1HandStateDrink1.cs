﻿using UnityEngine;

namespace _Moje
{
    public class L1HandStateDrink1 : AHandDrinkState
    {
        public L1HandStateDrink1(HandController handController) : base(handController)
        {
        }


        public override void Update()
        {
            //handController.GlassController.isHolding 
            //second buton

            handController.handTransform.RotateAround(handController.DrinkPivotRot.position, Vector3.left,
                40 * Time.deltaTime);
//                Debug.Log(handController.handTransform.rotation.eulerAngles.x);
            ;
            //Debug.Log(handController.handTransform.rotation.eulerAngles.x);
            if (handController.handTransform.rotation.eulerAngles.x < 280)
            {
                handController.HandReachedMouth();
            }
        }
    }
}