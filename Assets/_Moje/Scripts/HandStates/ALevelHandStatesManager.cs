
public abstract class ALevelHandStatesManager
{
   protected HandController HandController;

   protected ALevelHandStatesManager(HandController handController)
   {
      HandController = handController;
   }

   public abstract AHandGrabState GetLockedFromGrabbingState();
   
   public abstract AHandGrabState GetDrinkingState();
}
