﻿using UnityEngine;

namespace _Moje
{
    public class L3HandStateMove0:AHandMoveState
    {
        
        public Vector3 StartMouseClickPos { get; set; }
        private float lastMouseOffset;
        public Plane hitPlane;
        
        private Quaternion shiverRot;
        private float shiverTimeLimit = 0.1f;
        private float shiverTime;
        public L3HandStateMove0(HandController handController) : base(handController)
        {
            StartMouseClickPos = HandController.FAKE_EMPTY_VECTOR;
        }
     public override void Start()
        {
            base.Start();
            hitPlane =  new Plane(new Vector3(0, 1, 0),   handController.currentLevel.pointOnPlain);
            handController.SetRandomDirectionsForFinders();
        }


        public override void Update()
        {
           
            if (Input.GetMouseButton(0))
            {
                
                
                // isAnyMouseButtonDown = true;
                var mousePos = Input.mousePosition;
                //Debug.Log(mousePos);
                mousePos = handController._camera.ScreenToViewportPoint(mousePos);
                if (StartMouseClickPos == HandController.FAKE_EMPTY_VECTOR)
                {
                    StartMouseClickPos = new Vector3(mousePos.x, mousePos.y, 0);
//                    Debug.Log("on click");
                }
                
                Ray ray = handController._camera.ScreenPointToRay(Input.mousePosition);

                //Initialise the enter variable
                float enter = 0.0f;

                if (hitPlane.Raycast(ray, out enter))
                {
                    //Get the point that is clicked
            
                    Vector3 hitPoint = ray.GetPoint(enter);
//                    Debug.Log(hitPoint);
                   // hitPoint.z += Mathf.Sin(hitPoint.x) * 0.5f;
                    var limitPos = LimitPos(hitPoint);
                    handController.handTransform.position = Vector3.Lerp(handController.handTransform.position, limitPos, 0.02f) ;
                  //  Debug.DrawLine(handController._camera.transform.position, hitPoint);
                    //Move your cube GameObject to the point where you clicked
                    // m_Cube.transform.position = hitPoint;
                }

                
            }
            if(Input.GetMouseButtonUp(0))
            {

                StartMouseClickPos = HandController.FAKE_EMPTY_VECTOR;
            }

            handController.DrunkenFinger();
            //AddShivers();
        }

        void AddShivers()
        {
            if (Time.time - shiverTime > shiverTimeLimit)
            {
                shiverRot = Quaternion.Euler(0, 0, Random.Range(-20f, 20f));
                shiverTime = Time.time;
            }

            handController.handTransform.localRotation = Quaternion.Lerp(handController.handTransform.localRotation, shiverRot, 0.3f); 
        }

        Vector3 LimitPos(Vector3 pos)
        {
            
            if (pos.x > handController.currentLevel.planeLimits.y)
            {
                pos.x = handController.currentLevel.planeLimits.y;
            }
            if (pos.x < handController.currentLevel.planeLimits.x)
            {
                pos.x = handController.currentLevel.planeLimits.x;
            }
            if (pos.z < handController.currentLevel.planeLimits.z)
            {
                pos.z = handController.currentLevel.planeLimits.z;
            }
            if (pos.z > handController.currentLevel.planeLimits.w)
            {
                pos.z = handController.currentLevel.planeLimits.w;
            }

            return pos;
        }
        

        
    

    }
}