﻿using UnityEngine;

namespace _Moje
{
    public class L1HandStateGrab2:AHandGrabState
    {

        public L1HandStateGrab2(HandController handController) : base(handController)
        {
        }
        
        public override void Start()
        {
//            Debug.Log("lock");
            base.Start();
            handController.LockGlassToHand();
            // _trans = handController.GetGlassHandRelativeTransform();
        }
    }
}