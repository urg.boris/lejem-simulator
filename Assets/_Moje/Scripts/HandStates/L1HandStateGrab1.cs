﻿using UnityEngine;

namespace _Moje
{
    public class L1HandStateGrab1:AHandGrabState
    {

        
        public L1HandStateGrab1(HandController handController) : base(handController)
        {
        }

        public override void Start()
        {
            thumbContact = false;
            fingerContact = false;
        }


        public override void Update()
        {
            handController.GrabRutine(thumbContact, fingerContact);
        }

        public override bool IsInGrabbingMove()
        {
            return true;
        }
    }
}