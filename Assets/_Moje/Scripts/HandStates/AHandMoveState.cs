﻿using Newtonsoft.Json.Serialization;

public abstract class AHandMoveState: AHandMove
{
    protected AHandMoveState(HandController handController) : base(handController)
    {
    }
}