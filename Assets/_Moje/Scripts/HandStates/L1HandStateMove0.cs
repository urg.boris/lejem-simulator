﻿using UnityEngine;

namespace _Moje
{
    public class L1HandStateMove0:AHandMoveState
    {
        
        public Vector3 StartMouseClickPos { get; set; }
        private float reachingFinalHandPosT = 0;
        private float lastMouseOffset;
        public Plane hitPlane;
        public L1HandStateMove0(HandController handController) : base(handController)
        {
            StartMouseClickPos = HandController.FAKE_EMPTY_VECTOR;
        }

        public override void Start()
        {
            base.Start();
            hitPlane =  new Plane(new Vector3(0, 0, 1),   handController.currentLevel.pointOnPlain);
        }


        public override void Update()
        {
           
            if (Input.GetMouseButton(0))
            {
                
                
                // isAnyMouseButtonDown = true;
                var mousePos = Input.mousePosition;
                //Debug.Log(mousePos);
                mousePos = handController._camera.ScreenToViewportPoint(mousePos);
                if (StartMouseClickPos == HandController.FAKE_EMPTY_VECTOR)
                {
                    StartMouseClickPos = new Vector3(mousePos.x, mousePos.y, 0);
//                    Debug.Log("on click");
                }
                
                Ray ray = handController._camera.ScreenPointToRay(Input.mousePosition);

                //Initialise the enter variable
                float enter = 0.0f;

                if (hitPlane.Raycast(ray, out enter))
                {
                    //Get the point that is clicked
            
                    Vector3 hitPoint = ray.GetPoint(enter);
//                    Debug.Log(hitPoint);
                   // hitPoint.z += Mathf.Sin(hitPoint.x) * 0.5f;
                    var limitPos = LimitPos(hitPoint);
                    handController.handTransform.position = Vector3.Lerp(handController.handTransform.position, limitPos, 0.02f) ;
                  //  Debug.DrawLine(handController._camera.transform.position, hitPoint);
                    //Move your cube GameObject to the point where you clicked
                    // m_Cube.transform.position = hitPoint;
                }
/*
                lastMouseOffset = (StartMouseClickPos - mousePos).x;
              
                //reachingFinalHandPosT = addition;
                handController.SetHandPosFromAnimation(reachingFinalHandPosT+lastMouseOffset);*/
                
            }
            if(Input.GetMouseButtonUp(0))
            {
             //   Debug.Log("addition");
              //     Debug.Log(lastMouseOffset);
                reachingFinalHandPosT += lastMouseOffset;
             //      Debug.Log("reachingFinalHandPosT");
             //      Debug.Log(reachingFinalHandPosT);
                StartMouseClickPos = HandController.FAKE_EMPTY_VECTOR;
            }


           
        }

        Vector3 LimitPos(Vector3 pos)
        {
            
            if (pos.x > handController.currentLevel.planeLimits.y)
            {
                pos.x = handController.currentLevel.planeLimits.y;
            }
            if (pos.x < handController.currentLevel.planeLimits.x)
            {
                pos.x = handController.currentLevel.planeLimits.x;
            }
            if (pos.y > handController.currentLevel.planeLimits.w)
            {
                pos.y = handController.currentLevel.planeLimits.w;
            }
            if (pos.y < handController.currentLevel.planeLimits.z)
            {
                pos.y = handController.currentLevel.planeLimits.z;
            }

            return pos;
        }


    }
}