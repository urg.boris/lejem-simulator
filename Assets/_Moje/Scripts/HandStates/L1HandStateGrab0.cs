﻿using UnityEngine;

namespace _Moje
{
    public class L1HandStateGrab0:AHandGrabState
    {
        public L1HandStateGrab0(HandController handController) : base(handController)
        {
        }

        public override void Update()
        {
            if (Input.GetMouseButtonDown(2))
            {
                handController.SetGrabState(new L1HandStateGrab1(handController));
            }
            if(Input.GetKeyUp(KeyCode.A))
            {
                handController.SetGrabState(new L1HandStateGrab1(handController));
            }
            handController.FloatingFinger();
        }


    }
}