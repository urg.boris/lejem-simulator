﻿using UnityEngine;

namespace _Moje
{
    public class L1HandStateMove1:AHandMoveState
    {
        public Vector3 StartMouseClickPos { get; set; }
       // private float reachingFinalHandPosT = 0;
        private float lastMouseOffset;
        public Plane hitPlane;
        public L1HandStateMove1(HandController handController) : base(handController)
        {
            StartMouseClickPos = HandController.FAKE_EMPTY_VECTOR;
        }
        
        public override void Start()
        {
//            Debug.Log("rightplace");
            base.Start();
           // hitPlane =  new Plane(handController.handTransform.forward,   handController.currentLevel.pointOnPlain);
            
        }

        public override void Update()
        {
            hitPlane =  new Plane(handController.handTransform.up,   handController.DrinkPivotRot.position);
            /*
            //Debug.DrawRay(handController.handTransform.position, handController.handTransform.up, Color.magenta, 100);
            if (Input.GetMouseButton(0))
            {
                
                
                // isAnyMouseButtonDown = true;
                var mousePos = Input.mousePosition;

                //Debug.Log(mousePos);
                mousePos = handController._camera.ScreenToViewportPoint(mousePos);
                if (StartMouseClickPos == HandController.FAKE_EMPTY_VECTOR)
                {
                    StartMouseClickPos = new Vector3(mousePos.x, mousePos.y, 0);
//                    Debug.Log("on click");
                }
                
                Ray ray = handController._camera.ScreenPointToRay(Input.mousePosition);

                //Initialise the enter variable
                float enter = 0.0f;

                if (hitPlane.Raycast(ray, out enter))
                {
                    //Get the point that is clicked
            
                    Vector3 hitPoint = ray.GetPoint(enter);
//                    Debug.Log(hitPoint);
                   // hitPoint.z += Mathf.Sin(hitPoint.x) * 0.5f;
                    var limitPos = LimitPos(hitPoint);
                    handController.handTransform.position = Vector3.Lerp(handController.handTransform.position, limitPos, 0.02f) ;
                  //  Debug.DrawLine(handController._camera.transform.position, hitPoint);
                    //Move your cube GameObject to the point where you clicked
                    // m_Cube.transform.position = hitPoint;
                }

                
            }
            if(Input.GetMouseButtonUp(0))
            {
             //   Debug.Log("addition");
              //     Debug.Log(lastMouseOffset);
                reachingFinalHandPosT += lastMouseOffset;
             //      Debug.Log("reachingFinalHandPosT");
             //      Debug.Log(reachingFinalHandPosT);
                StartMouseClickPos = HandController.FAKE_EMPTY_VECTOR;
            }*/
            
            if (Input.GetMouseButton(0))
            {
                
                
                // isAnyMouseButtonDown = true;
                var mousePos = Input.mousePosition;
                //Debug.Log(mousePos);
                mousePos = handController._camera.ScreenToViewportPoint(mousePos);
                if (StartMouseClickPos == HandController.FAKE_EMPTY_VECTOR)
                {
                    StartMouseClickPos = new Vector3(mousePos.x, mousePos.y, 0);
//                    Debug.Log("on click");
                }
                
                Ray ray = handController._camera.ScreenPointToRay(Input.mousePosition);

                //Initialise the enter variable
                float enter = 0.0f;

                if (hitPlane.Raycast(ray, out enter))
                {
                    //Get the point that is clicked
            
                    Vector3 hitPoint = ray.GetPoint(enter);
//                    Debug.Log(hitPoint);
                    // hitPoint.z += Mathf.Sin(hitPoint.x) * 0.5f;
                  //  var limitPos = LimitPos(hitPoint);
                    handController.handTransform.position = Vector3.Lerp(handController.handTransform.position, hitPoint, 0.02f) ;
                    //  Debug.DrawLine(handController._camera.transform.position, hitPoint);
                    //Move your cube GameObject to the point where you clicked
                    // m_Cube.transform.position = hitPoint;
                }

                
            }
            if(Input.GetMouseButtonUp(0))
            {
                //   Debug.Log("addition");
                //     Debug.Log(lastMouseOffset);
             //   reachingFinalHandPosT += lastMouseOffset;
                //      Debug.Log("reachingFinalHandPosT");
                //      Debug.Log(reachingFinalHandPosT);
                StartMouseClickPos = HandController.FAKE_EMPTY_VECTOR;
            }


            //handController.FloatingFinger();
        }



    }
}